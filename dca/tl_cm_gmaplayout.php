<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Christian Muenster 2012
 * @author     Christian Muenster
 * @package    cm_membergooglemaps
 * @license    LGPL
 * @filesource
 */

/**
 * palette for tl_cm_gmaplayout
 */

$GLOBALS['TL_DCA']['tl_cm_gmaplayout'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ctable'                      => array('tl_cm_gmaptypestyle'),
		'switchToEdit'                => true,
		'enableVersioning'            => true,
		'onload_callback' => array
		(
			array('tl_cm_gmaplayout', 'checkPermission'),
			array('tl_cm_gmaplayout', 'updateMapLayout')
		),
		'oncopy_callback' => array
		(
			array('tl_cm_gmaplayout', 'scheduleUpdate')
		),
		'onsubmit_callback' => array
		(
			array('tl_cm_gmaplayout', 'scheduleUpdate')
		),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'name' => 'unique'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
//			'mode'                    => 1,
//			'fields'                  => array('name'),
//			'panelLayout'             => 'filter,search,limit',
//			'headerFields'            => array('name'),
//			'child_record_callback'   => array('tl_cm_gmaplayout', 'listLayout'),
//			'child_record_class'      => 'no_padding'
			'mode'                    => 1,
			'fields'                  => array('name'),
			'flag'                    => 1,
			'panelLayout'             => 'limit'
		),
		'label' => array
		(
			'fields'                  => array('name'),
			'format'                  => '%s'
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['edit'],
				'href'                => 'table=tl_cm_gmaptypestyle',
				'icon'                => 'edit.gif',
				'attributes'          => 'class="contextmenu"'
			),
			'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['editheader'],
				'href'                => 'table=tl_cm_gmaplayout&act=edit',
				'icon'                => 'header.gif',
				'button_callback'     => array('tl_cm_gmaplayout', 'editHeader'),
				'attributes'          => 'class="edit-header"'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['copy'],
//				'href'                => 'act=paste&mode=copy',
				'href'                => 'act=copy',
				'icon'                => 'copy.gif',
				'button_callback'     => array('tl_cm_gmaplayout', 'copyMapLayout')
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{title_legend},name'
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
//			'foreignKey'              => 'tl_theme.name',
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
//			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'sortOrder' => array
		(
			'sql'                     => "varchar(32) NOT NULL default ''"
		),
		'name' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_cm_gmaplayout']['name'],
			'inputType'               => 'text',
			'exclude'                 => true,
			'search'                  => true,
			'flag'                    => 1,
			'eval'                    => array('mandatory'=>true, 'unique'=>true, 'rgxp'=>'alnum', 'maxlength'=>64, 'spaceToUnderscore'=>true, 'tl_class'=>'w50'),
			'sql'					  => "varchar(255) NOT NULL default ''"
		)
	)

);


class tl_cm_gmaplayout extends Backend
{
	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	 * Check permissions to edit the table
	 */
	public function checkPermission()
	{
		if ($this->User->isAdmin)
		{
			return;
		}

		if (!$this->User->hasAccess('edit', 'maplayout'))
		{
			$this->log('Not enough permissions to access the mapLayout module', 'tl_cm_gmaplayout checkPermission', TL_ERROR);
			$this->redirect('contao/main.php?act=error');
		}
	}


	/**
	 * Check for modified map layout and update them if necessary
	 */
	public function updateMapLayout()
	{
		$session = $this->Session->get('maplayout_updater');

		if (!is_array($session) || empty($session))
		{
			return;
		}

    //$this->import('MapLayout');
    	$this->MapLayout = new ChrMue\cm_GoogleMaps\MapLayout(); 
		foreach ($session as $id)
		{
			$this->MapLayout->updateMapLayout($id);
		}

		$this->Session->set('maplayout_updater', null);
	}

	/**
	 * Schedule a map layout update
	 * 
	 * This method is triggered when a single style sheet or multiple style
	 * sheets are modified (edit/editAll) or duplicated (copy/copyAll).
	 * @param mixed
	 */
	public function scheduleUpdate($id)
	{
		// The onsubmit_callback passes a DataContainer object
		if (is_object($id))
		{
			$id = $id->id;
		}
		// Return if there is no ID 
		if (!$id || \Input::get('act') == 'copy')
		{
			return;
		}

		// Store the ID in the session
		$session = $this->Session->get('maplayout_updater');
		$session[] = $id;
		$this->Session->set('maplayout_updater', array_unique($session));
	}


	/**
	 * List a style sheet
	 * @param array
	 * @return string
	 */
	public function listLayout($row)
	{
  print_r($row); die();
  
  	return '<div style="float:left">'. $row['name']  . "</div>\n";
	}


	/**
	 * Return the edit header button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function editHeader($row, $href, $label, $title, $icon, $attributes)
	{
    return ($this->User->isAdmin || count(preg_grep('/^tl_cm_gmaplayout::/', $this->User->alexf)) > 0) ? '<a href="'.$this->addToUrl($href.'&id='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.$this->generateImage($icon, $label).'</a> ' : '';
	}
	/**
	 * Return the copy mapLayout button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function copyMapLayout($row, $href, $label, $title, $icon, $attributes)
	{
		return ($this->User->isAdmin || $this->User->hasAccess('create', 'maplayout')) ? 
			'<a href="'.$this->addToUrl($href.'&id='.$row['id']).'" title="'.specialchars($title).'"'.$attributes.'>'.$this->generateImage($icon, $label).'</a> ' : 
			$this->generateImage(preg_replace('/\.gif$/i', '_.gif', $icon)).' ';
	}
}

?>