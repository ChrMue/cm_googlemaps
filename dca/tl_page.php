<?php

/**
 * Extension for the Contao Open Source CMS
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2018
 * @author     Christian Muenster
 * @package    CM_GoogleMaps
 * @license    LGPL
 */
/**
 * palette for tl_page
 */

/**
 * palette for tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][]  = 'cm_gm_acceptance_required';

$GLOBALS['TL_DCA']['tl_page']['palettes']['root'] .= ';cm_map_apikey;{cm_gm_accepance_legend:hide},cm_gm_acceptance_required';

$GLOBALS['TL_DCA']['tl_page']['subpalettes']['cm_gm_acceptance_required'] ='cm_gm_acceptance_text';


/**
 * Add fields to tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['fields']['cm_map_apikey'] = array (
    'label' => &$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => false,'maxlength' => 255),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['cm_gm_acceptance_required'] = array (
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_required'],
    'exclude'   => true,
    'default'   => false,
    'inputType' => 'checkbox',
    'default'   => '',
    'eval'      => array('submitOnChange' => true, 'tl_class' => 'clr m12'),
    'sql'       => "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_page']['fields']['cm_gm_acceptance_text'] = array (
    'label'       => &$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_text'],
    'exclude'     => true,
    'search'      => true,
    'inputType'   => 'textarea',
    'eval'        => array('rte' => 'tinyMCE', 'helpwizard' => true),
    'explanation' => 'insertTags',
    'sql'         => "text NULL",
);