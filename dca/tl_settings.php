<?php 
/**

 * @copyright  Christian Muenster 2017 
 * @author     Christian Muenster 
 * @package    CM_MemberGoogleMaps
 * @license    LGPL 
 * @filesource
*/

/**
 * Class tl_settings_cm_googlemaps
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Christian Muenster 2017 
 * @author     Christian Muenster 
 * @package    Controller
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
 */



$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{cm_googlemaps},cm_requestlimit,cm_request_gm_ssl,cm_map_apikey'; 

/**
 * Add fields
 */
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_requestlimit'] = array(
	    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_requestlimit'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'rgxp'=>'natural', 'minval'=>0, 'nospace'=>true, 'tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_request_gm_ssl'] = array(
	    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_request_gm_ssl'],
			'exclude'                 => true,
			'inputType'               => 'checkbox',
			'eval'                    => array('tl_class'=>'w50 m12')
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_map_apikey'] = array(
	    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_map_apikey'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('tl_class'=>'w50 m12')
);


?>