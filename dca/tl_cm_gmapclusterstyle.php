<?php


/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2018 Leo Feyer
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2018
 * @author     Christian Muenster
 * @package    cm_googlemaps
 * @license    LGPL
 * @filesource
 */


$GLOBALS['TL_DCA']['tl_cm_gmapclusterstyle'] = array
(
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ptable'                      => 'tl_cm_gmapclusterlayout',
		'enableVersioning'            => true,

		'onload_callback' => array
		(
			array('tl_cm_gmapclusterstyle', 'checkPermission'),
		),

		'onrestore_callback' => array
		(
			array('tl_cm_gmapclusterstyle', 'updateAfterRestore')
		),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 4,
			'fields'                  => array('sorting'),
			'panelLayout'             => 'filter,search,limit',
			'headerFields'            => array('name','tstamp'),
//			'child_record_callback'   => array('MapLayout', 'compileDefinition')
			'child_record_callback'   => array('tl_cm_gmapclusterstyle', 'listStyles'),


		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['copy'],
				'href'                => 'act=paste&amp;mode=copy',
				'icon'                => 'copy.gif',
				'attributes'          => 'onclick="Backend.getScrollOffset()"'
			),
			'cut' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['cut'],
				'href'                => 'act=paste&amp;mode=cut',
				'icon'                => 'cut.gif',
				'attributes'          => 'onclick="Backend.getScrollOffset()"'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['toggle'],
				'icon'                => 'visible.gif',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s,\'tl_cm_gmaptypestyle\')"',
				'button_callback'     => array('tl_cm_gmapclusterstyle', 'toggleIcon')
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'      => array('settextcolor','seturl','setsize'),
		'default'           => '{cm_clusterstylecomment_legend},comment;{cm_clusterstyle_legend},settextcolor,seturl,setsize;'
  ),

	// Subpalettes
 	'subpalettes' => array
 	(
    'settextcolor'   => 'textcolor', 
    'seturl'         => 'singleSRC,url', 
    'setsize'        => 'width,height', 
  ),
                                 
  // Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'           => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'    => 'tl_cm_gmapclusterlayout.name',
			'sql'           => "int(10) unsigned NOT NULL default '0'",
			'relation'      => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'sorting' => array
		(
			'sql'           => "int(10) unsigned NOT NULL default '0'"
		),
		'tstamp' => array
		(
			'sql'           => "int(10) unsigned NOT NULL default '0'"
		),
		'invisible' => array
		(
			'label'         => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['invisible'],
			'sql'           => "char(1) NOT NULL default ''"
		),

    'comment' => array
    (
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['comment'],
		'inputType'         => 'text',
//		'eval'              => array('maxlength'=>50, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
		'eval'              => array('maxlength'=>64, 'tl_class'=>'w50'),
		'sql'				=> "varchar(64) NOT NULL default ''"
    ),
    'settextcolor' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['settextcolor'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
			
    ),
    'textcolor' => array
    (
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['textcolor'],
		'inputType'         => 'text',
//		'eval'              => array('maxlength'=>50, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
		'eval'              => array('maxlength'=>64, 'multiple'=>false, 'size'=>1,'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
		'sql'				=> "varchar(64) NOT NULL default ''"
    ),
    'seturl' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['seturl'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
			
    ),
	'singleSRC' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['singleSRC'],
		'exclude'           => true,
		'inputType'         => 'fileTree',
		'eval'              => array('filesOnly'=>true, 'fieldType'=>'radio', 'tl_class'=>'clr w50'),
//		'load_callback'     => array
//		(
//			array('tl_content', 'setSingleSrcFlags')
//		),
//		'save_callback'     => array
//		(
//			array('tl_content', 'storeFileMetaInformation')
//		),
		'sql'               => "binary(16) NULL"
	), 
    'url' => array
    (
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['url'],
		'inputType'         => 'text',
//		'eval'              => array('maxlength'=>50, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
		'eval'              => array('maxlength'=>512, 'multiple'=>false, 'tl_class'=>'w50'),
		'sql'				=> "varchar(512) NOT NULL default ''"
    ),
    'setsize' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['setsize'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
			
    ),
    'width' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['width'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'clr w50'),
		'sql'				=> "int(10) NOT NULL default '0'"
			
    ),
    'height' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmapclusterstyle']['height'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default '0'"
			
    )
  )

);



class tl_cm_gmapclusterstyle extends Backend
{

	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


  public function checkPermission(){
		if ($this->User->isAdmin)
		{
			return;
		}
// 		if (!$this->User->hasAccess('xxx', 'xxxxxxx'))
// 		{
// 			$this->log('Not enough permissions to access the map type style module',
//         'tl_cm_gmaptypestyle checkPermission', TL_ERROR);
// 			$this->redirect('contao/main.php?act=error');
// 		}
		
   }


	/**
	 * Return the color picker wizard
	 * @param DataContainer
	 * @return string
	public function colorPicker(DataContainer $dc)
	{
		return ' ' . $this->generateImage('pickcolor.gif', $GLOBALS['TL_LANG']['MSC']['colorpicker'], 'style="vertical-align:top;cursor:pointer" id="moo_'.$dc->field.'"') . '
  <script>
  new MooRainbow("moo_'.$dc->field.'", {
    id:"ctrl_' . $dc->field . '",
    startColor:((cl = $("ctrl_' . $dc->field . '").value.hexToRgb(true)) ? cl : [255, 0, 0]),
    imgPath:"plugins/colorpicker/images/",
    onComplete: function(color) {
      $("ctrl_' . $dc->field . '").value = color.hex.replace("#", "");
    }
  });
  </script>';
	}
	 */

	public function listStyles($row)
	{
		$code = sprintf('<div style="float:left"><strong>%s</strong>',$row['comment']);
		$code .='<div class="cm_mapclustermarkers">';
		$code.='</div></div>';
		return $code;
	}

  	/**
	 * Return the "toggle visibility" button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		if (strlen(Input::get('tid')))
		{
			$this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1));
			$this->redirect($this->getReferer());
		}

		$href .= '&amp;tid='.$row['id'].'&amp;state='.$row['invisible'];

		if ($row['invisible'])
		{
			$icon = 'invisible.gif';
		}		

		return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.$this->generateImage($icon, $label).'</a> ';
	}


	/**
	 * Toggle the visibility of a format definition
	 * @param integer
	 * @param boolean
	 */
	public function toggleVisibility($intId, $blnVisible)
	{
		$this->createInitialVersion('tl_cm_gmapclusterstyle', $intId);
	
		// Trigger the save_callback
		if (is_array($GLOBALS['TL_DCA']['tl_cm_gmapclusterstyle']['fields']['invisible']['save_callback']))
		{
			foreach ($GLOBALS['TL_DCA']['tl_cm_gmapclusterstyle']['fields']['invisible']['save_callback'] as $callback)
			{
				$this->import($callback[0]);
				$blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
			}
		}

		// Update the database
		$this->Database->prepare("UPDATE tl_cm_gmapclusterstyle SET tstamp=". time() .", invisible='" . ($blnVisible ? '' : 1) . "' WHERE id=?")
					   ->execute($intId);
        
		$this->createNewVersion('tl_cm_gmapclusterstyle', $intId);
		$this->log('A new version of record "tl_cm_gmapclusterstyle.id='.$intId.'" has been created'.$this->getParentRecords('tl_cm_gmapclusterstyle', $intId), 'tl_cm_gmapclusterstyle toggleVisibility()', TL_GENERAL);

		// Recreate the style sheet
		$objClusterlayout = $this->Database->prepare("SELECT pid FROM tl_cm_gmapclusterstyle WHERE id=?")
									    ->limit(1)
									    ->execute($intId);

		if ($objClusterlayout->numRows)
		{
			//$this->import('MapLayout');
		    $this->ClusterLayout= new ChrMue\cm_GoogleMaps\ClusterLayout(); 
			$this->ClusterLayout->updateClusterLayout($objClusterlayout->pid);
		}
	}

}
