<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2018 Leo Feyer
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2018
 * @author     Christian Muenster
 * @package    cm_googlemaps
 * @license    LGPL
 * @filesource
 */

namespace ChrMue\cm_GoogleMaps;
/**
 * Class MapLayout
 *
 * Provide methods to handle style sheets.
 * @copyright  Christian Münster 2012
 * based on classes and methods to generate css by Leo Feyer <http://www.contao.org>
 * @package    Controller
 */
class ClusterLayout extends \Frontend
{

    private $defWidth = array(53,56,66,78,90);
	/**
	 * Import String library
	 */
	public function __construct()
	{
		parent::__construct();

		if (!version_compare(VERSION, '3.5', '<'))
			$this->import('StringUtil');
		else {
			$this->import('String');
		}
		$this->import('Files');
	}


	/**
	 * Update a Map Layout
	 * @param integer
	 */

	private function getDefinition($row, $path, $ext, $n)
	{
        if ($row->seturl)
        {
            if ($row->url)
            {
                $url=$row->url;
            }
            if ($row->singleSRC) 
            {	
                $objFile = \FilesModel::findByUuid($row->singleSRC);
    
    			if ($objFile !== null)
    			{
    				$url = $objFile->path;
    			};
            }
        } 
        else
        {
            $url = $path.$n.'.'.$ext;
        }
        $textcolor=($row->settextcolor && $row->textcolor)?$row->textcolor:'';
        
        $width=$this->defWidth[$n-1];
        if ($row->setsize)
        {
            $width=$row->width;
            $height = $row->height;
        }
        if ($height==0) $height=$width;     
        // die("xx");
        $return.="{"
            .($textcolor?"textColor: '#".$textcolor."',":"")
            ."url: '".$url."'"
            .",width: ".$width
            .",height: ".$height
            ."}";
       //echo $return; die();
       return $return;
	}
 
	private function getDefinitionDirectOnTrue($r,$type,$style,$isColor,$styleArr)
	{
		
		if ($r[$type."_".$style]==1)
			$styleArr[]= "{".$style.": true}";
    return $styleArr;
	}
	/**
	 * Compile format definitions and return them as string
	 * @param array
	 * @param boolean
	 * @param array
	 * @return string
	 */
	public function compileDefinition($id)
	{
    
        $layoutData = $this->getLayout($id);
        $iconsdef = $this->getStyles($id);
        $path = $layoutData->imagePath;
        $ext = $layoutData->imageExtension;
		$return = "var clusterStyles = [";
        $first=true;
        $n=0;
		foreach ($iconsdef as $x) {
		    if ($n>0) 
            {
                $return .=",";
            }
            $n++;
			$return.=$this->getDefinition($iconsdef,$path, $ext,$n);
            $return .=$stylersArr; 
		}
        return $return."];";
        
	}

    private function getLayout($id)
    {
       $data = GoogleMapsClusterLayoutModel::findById($id);
       return $data;
    }
    private function getStyles($pid)
    {
       $data = GoogleMapsClusterStyleModel::findByParent($pid);
       return $data;
    }
/*

                        $objClusterLayout = $this -> Database 
                        	-> prepare("SELECT * FROM tl_cm_gmapclusterlayout WHERE id = ?")->limit(1) 
                        	-> execute($this -> cm_map_clusterlayoutid);
    
                        $objClusterLayout -> next;
                        $objClusterIcons = $this -> Database 
                        	-> prepare("SELECT * FROM tl_cm_gmapclusterstyle WHERE pid = ?") 
                        	-> execute($this -> cm_map_clusterlayoutid);
                        print_r($objClusterLayout);             
                        echo "--------------------------------";
                        $objClusterIcons -> next;
                        print_r($objClusterIcons);             
*/

}
