<?php 

namespace ChrMue\cm_GoogleMaps;

/*
 * @copyright  Christian Muenster 2009 
 * @author     Christian Muenster 
 * @package    library for module CM_MemberGoogleMaps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
*/

interface Icm_connection {
    // Die Methode wird über das Interface nur vorgeschrieben,
    // daher darf sie keinen Inhalt haben.
    public function download($host,$ssl,$url,$moreOptions);
    public function checkInstalled();
}
 

class cm_mgm_fSockOpenConnect implements Icm_connection
{
	private function fSockOpendownload($host, $ssl, $url)
	{

    // 'http://maps.google.com/maps/geo?output=xml&key='.$gm_refid.'&oe=utf-8&q='
 
	if($ssl){
	    $host = "ssl://".$host;
    	$port = 443;
	}
  	else $port = 80;
     
      $fsock = fsockopen($host, $port);
      if ($fsock) {
        fwrite($fsock, "GET " . $url . "\r\nHost: " . $host . "\r\nConnection: Close\r\n\r\n");
        $body = false;
        $data = '';
        WHILE(!feof($fsock)) {
            //$data .= fgets($fsock, 4096) . "\n";
            //$data .= fread($fsock, 1024);
          $s = fgets($fsock, 1024);
          if ( $body )
              $data .= $s;
          if ( $s == "\r\n" )
              $body = true;
        }
        fclose($fsock);
        return $data;
      }
      else
        //$this->sys->log("FAIL: fsockopen Functions","cm_membergooglemaps",TL_GENERAL);
        return null;
	}

    function checkInstalled()
    {
        return true;
    }

	function download($host,$ssl,$url,$moreOptions)
	{
//   		$fullurl=$url."kml".$moreOptions;
   		$fullurl=$url.$moreOptions;
	    $daten = $this->fSockOpendownload($host,$ssl,$fullurl);
   		$matches = array();
		
   		//preg_match_all('/<c o="([^"]*)" l="([^"]*)" s="([^"]*)">([^<]*)<\/c>/', $daten, $matches, PREG_SET_ORDER);
//  		preg_match_all('/^[^<]*(<kml.*<\/kml>)[^>]*$/', $daten, $matches, PREG_SET_ORDER);
//  		preg_match_all('/<coordinates>*.*<\/coordinates>/', $daten, $matches);
//  		preg_match('/<coordinates>*.*<\/coordinates>/', $daten, $matches);
      //die();
//      return $matches;
        return $daten;    
	}
}

class cm_mgm_cURLConnect implements Icm_connection
{
    
    public function cURLcheckBasicFunctions()
	{
  		if( !function_exists("curl_init") ||
      		!function_exists("curl_setopt") ||
      		!function_exists("curl_exec") ||
      		!function_exists("curl_close"))
      {
        return false;
      }
  		else
			return true;
	}

	private function cURLdownload($url,$ssl)
	{
		$url = ($ssl?'https://':'http://').$url;		

 		$ch = curl_init();
  		if($ch)
  		{
    		if( !curl_setopt($ch, CURLOPT_URL, $url) ) {
    		  throw new \Exception("FAIL: curl_setopt(CURLOPT_URL)");
            return null;
            }
    		if( !curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ) { 
    		  throw new \Exception("FAIL: curl_setopt(CURLOPT_FILE)");
            return null;
    		}
            if( !curl_setopt($ch, CURLOPT_HEADER, 0) ) {
                throw new \Exception("FAIL: curl_setopt(CURLOPT_HEADER)");
            return null;
            }
    		$retvalue =curl_exec($ch);
    		curl_close($ch);
    		return $retvalue;
  		}
  		else  
        {
          throw new \Exception("FAIL: curl_init()");
          return null;
        }
	}
    
    function checkInstalled()
    {
        return $this->cURLcheckBasicFunctions();
    }

	function download($host,$ssl,$url,$moreOptions)
	{
		if( !$this->cURLcheckBasicFunctions() ) {
		    throw new \Exception("UNAVAILABLE: cURL Basic Functions");
		    return null;
	    }
	    $fullurl=$host.$url.$moreOptions;
		$daten = $this->cURLdownload($fullurl,$ssl);
		return $daten;
	}
}

class GoogleMapsXMLData
{
	private $URL_Base;
//	private $refid;
    private $accessObj;

	private $todo;
	private $ssl=false;
	
	public function setConnection(Icm_connection $connection)
	{
        $this->accessObj =$connection;
    }
	
    public function setPath($host,$ssl,$path,$moreOptions)
	{
		$this->ssl= $ssl?true:false;
		$this->URL_Base=$path."xml";
		$this->Host=$host;
		$this->moreOptions=$moreOptions;
	}

//	function __construct($refid)

	function __construct()
	{
//		$this->refid=$refid;
		$this->setPath('',false,'','');
   	    $this->setConnection(new cm_mgm_cURLConnect());
 	    if (!$this->accessObj->cURLcheckBasicFunctions()) 
        {
            $this->setConnection(new cm_mgm_fSockOpenConnect());
        }
	}

	function readXML($Location,$country=null)
	{
    	$tempurl= "?".($this->moreOptions ? $this->moreOptions."&" : "");
		if ($Location)
		{
          $tempurl .= $Location.($country?"&components=country:".$country:"");
          //die($this->Host.$this->URL_Base.$tempurl);
          
          
          $daten=$this->accessObj->download($this->Host,$this->ssl, $this->URL_Base,$tempurl);
          //print_r($daten); die();
          if ($daten) { $xmlData = new \SimpleXMLElement($daten); }
        }
		return $xmlData;
	}

}
