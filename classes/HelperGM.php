<?php
namespace ChrMue\cm_GoogleMaps;

class HelperGM extends \Backend
{
	protected static $strTable = null;

	private function checkTable($fctName)
	{
	    if (static::$strTable!=null && \Database::getInstance()->tableExists(static::$strTable)) return true;
	    
	    $this->log("table not defined" ,$fctName,TL_GENERAL);
	    	//throw new \Exception("table not defined");
	    return false;
	}
	
	protected function getGMGeoData($id,$locationData, $oldvalue, $fieldmapping, $key)
    {
        $varValue = $oldvalue;

		if (!$this->checkTable("getGMGeoData"))return $varValue; 

        $t = static::$strTable;
		$count= $locationData->cm_googlemaps_requestcount;
        //street,postal,city
        if ($locationData)
        {
            $lat=null;
            $lng=null;
            $status=null;
            
            $doRefresh = false;
            $doRequest=false;
            // continue if automatic geocoding ist required
            // otherwise leave coordinates unchanged
            if ($locationData->cm_googlemaps_autocoords)
            {
                $apiKey= ($key ? $key : \config::get('cm_map_apikey'));
                
                $var = $fieldmapping['postal'];
                $location = str_replace(' ', '+', ($locationData->$var));
                $var = $fieldmapping['city'];
                if ($locationData->$var)
                {
                    if ($location) $location .= '+';
                    $location .= str_replace(' ', '+', ($locationData->$var));
                }
                $var =  $fieldmapping['street'];
                if ($locationData ->$var)
                {
                    if ($location) $location .= ',';
                    $location .= str_replace(' ', '+', ($locationData->$var));
                }
                $var =$fieldmapping['country'];
                if ($locationData->$var)
                {
                    $country = $locationData->$var;
                } 
				$useSSL = \Config::get('cm_request_gm_ssl') || \Environment::get('ssl');
				//$this->log("Location (" . $locationData->id . ") " . $location.' SSL: '.($useSSL?"yes":"no"), __METHOD__, TL_GENERAL);
				
				
				$data = cm_GoogleMap_lib::getGoogleMapsGeoData("&address=" . $location, $useSSL,$country,$apiKey);
                if ($data)
                {
                    if (array_key_exists("lat",$data))
                    {
                        $lat = $data["lat"];
                        $lng = $data["lng"];
                        $varValue =$lat.','.$lng;
                        $status = $data["status"];
                        
                        $this->log("Finding location (".$t.") of id " . $locationData->id . ": " . "data: " . $lat.','.$lng.' ('.$data["status"].')', __METHOD__, TL_GENERAL);
                        
                        $doRequest=true;
                        $doRefresh = ($varValue != $oldvalue ||
                            $locationData->cm_googlemaps_lat != $lat ||
                            $locationData->cm_googlemaps_lng != $lng
                            );
                        
                    }
                    else
                    {
                        $this->log($data['status'].': '.$data['error_message'], __METHOD__, TL_ERROR);
                    }
                }
				$count++;
            } else
            {
            	$doRefresh = ($varValue != $oldvalue ||
            	($locationData->cm_googlemaps_lat.','.$locationData->cm_googlemaps_lng)!=$varValue);
         		list($lat, $lng, $altitude) = explode(',', $varValue);
		    }
            if ($doRefresh)
            {
            	$arr = array('cm_googlemaps_coords' => $varValue, 
                			 'cm_googlemaps_lat' => $lat, 
                			 'cm_googlemaps_lng' => $lng);
                \Database::getInstance()->prepare("UPDATE ".$t." %s"
//                 			.($doRequest?",cm_googlemaps_requestcount=cm_googlemaps_requestcount+1":"")
                			." WHERE id=?")->set($arr)->execute($locationData->id);
        		//$this->log($t.": id ".$id.": ".$varValue ." (Anfrage ".$count.")->".$status, __METHOD__, TL_GENERAL);
        		$this->log(sprintf("%s: id %s: %s (Anfrage %s)->%s",
							$t,
							$id,
							$varValue,
							$count,
							$status),
							__METHOD__, TL_GENERAL
						  );
            }
        }
   		return $varValue;
    }
	
    protected function doGeoRouting($dc)
    {
        if (!$this->checkTable("geoRouting"))return; 

        $t = static::$strTable;
		$fieldmapping =$this->getFieldMapping();

    	$limit = \Config::get('cm_requestlimit');
        if (!$limit) $limit=3;
		$id=null;
		if ($dc && $dc->id)
		{
			$id=$dc->id;
			$msg = ' for parent id='.$id;
		}

		$this->log("Finding locations (".$t.") started".($id?$msg:"")."...", __METHOD__, TL_GENERAL);
        //    $additionalFields[]=null;

		$objLocation = \Database::getInstance()->prepare('SELECT * FROM '.$t.' WHERE cm_googlemaps_autocoords=1 '
                                .'AND (coalesce(cm_googlemaps_requestcount,0)<='.$limit.') AND (cm_googlemaps_coords="," ' . ' OR cm_googlemaps_coords="" )'
                                .($id?'AND pid='.$id:'')
								)->execute();
        if ($objLocation->numRows)
        {
            $apiKey=\Config::get('cm_map_apikey');
            //$this->log("Update of " . $objLocation->numRows . " Member updaten", __METHOD__, TL_GENERAL);
            while ($objLocation->next())
            {
                //$this->log("Item: "  "(" . $objLocation->id . ")", __METHOD__, TL_GENERAL);
                $varValue = $this->getGMGeoData($objLocation->id, $objLocation, $objLocation->cm_googlemaps_coords, $fieldmapping,$apiKey);
            }
        }
		$this->log("Finding locations (".$t.") finished", __METHOD__, TL_GENERAL);
    }
    
    public function cronGeoRouting(){
        $this->doGeoRouting($dc);
    }

    protected function geoRouting($dc){
        $this->doGeoRouting($dc);
        $this->redirect($this->getreferer());
    }

      /**
   * get memberdata (currently stored in the database)
   * and perform google geocoding
   */
    private function getFieldMappingOfField($key,$mapping,$fields)
	{
		if (key_exists($key,$mapping) && $mapping[$key] && 
			key_exists($mapping[$key],$GLOBALS['TL_DCA'][static::$strTable]['fields']))
		{
			$varValue= $mapping[$key];
			$varValue = \Input::decodeEntities($varValue);
			$varValue = \Input::xssClean($varValue, true);
			$varValue = \Input::stripTags($varValue);
			return $varValue;
		}
		return $key;
	}
	
	private function getFieldMapping()
	{
	    $t = static::$strTable;
		$mapping = $GLOBALS['TL_DCA'][$t]['config']['cm_gm_mapping'];
	
		$street = 'street';
		$postal = 'postal';
		$city = 'city';
		$country ='country';
		$has_mapping=false;
	
		if ($mapping && is_array($mapping))
		{
			$street=$this->getFieldMappingOfField('street',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
			$postal=$this->getFieldMappingOfField('postal',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
			$city=$this->getFieldMappingOfField('city',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
			$country=$this->getFieldMappingOfField('country',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
		}
		return array(
			'street'=>$street,
			'postal'=>$postal,
			'city'=>$city,
			'country'=>$country
			);
	}

	public function getGMGeoDataBEBase($varValue, $dc, $key)
	{
	    if (!$this->checkTable("getGMGeoDataBE"))return $varValue;
	    
	    $t = static::$strTable;
	    $fieldmapping =$this->getFieldMapping();
	    $street=$fieldmapping['street'];
	    $postal=$fieldmapping['postal'];
	    $city=$fieldmapping['city'];
	    $country=$fieldmapping['country'];
	    
	    //street,postal,city
	    $objLocation = \Database::getInstance()->prepare("SELECT id,$street,$postal,$city,$country,cm_googlemaps_lat,"
	        ."cm_googlemaps_lng,cm_googlemaps_autocoords,cm_googlemaps_requestcount FROM ".$t." WHERE id=?")
	        ->limit(1)
	        ->execute($dc->id);
	        
	        
	        if ($objLocation->numRows)
	        {
	            $data=$this->getGMGeoData($objLocation->id,$objLocation,$varValue,$fieldmapping,$key);
	            
	            $varValue=$data; //['lat'].','.$data['lng'];
	            // otherwise coordinates will be unchanged
	        }
	        return $varValue;
	}
	
	public function getGMGeoDataBE($varValue, $dc)
	{
	    $apiKey= \config::get('cm_map_apikey');
	    return $this->getGMGeoDataBEBase($varValue, $dc, $apiKey);
	}

	public function getGMGeoDataFE($varValue, $dc=null)
	{
		if (TL_MODE == 'BE') return $varValue;
		//$root_id = $this->getRootIdFromUrl();
		$root_id = \Frontend::getRootPageFromUrl()->id;
		$root_details =  \PageModel::findWithDetails($root_id);
		$apiKey = $root_details->cm_map_apikey;
		if (!$apiKey) $apiKey=\Config::get('cm_map_apikey'); 
		return $this->getGMGeoDataBEBase($varValue->cm_googlemaps_coords, $varValue, $apiKey);           
   	}

	public function resetCounterBE($varValue, $dc)
	{
		$t = static::$strTable;	
	    $this->log("Reset request (".$t.") counter".($varValue?" from ".$varValue:"")." to 0", __METHOD__, TL_GENERAL);
        $varValue = 0;
        return $varValue;
	}
	
	public function resetCounterFE($varValue, $dc)
    {
		if (TL_MODE == 'BE') return $varValue; 
        return $this->resetCounterBE($varValue->cm_googlemaps_requestcount, $dc);
    }
   
}

