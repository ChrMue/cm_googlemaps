<?php 
/**
 * TL_ROOT/system/modules/cm_googlemaps/languages/de/tl_module.php 
 * 
 * Contao extension: cm_googlemaps 1.3.0 rc2 
 * 
 * Copyright : © 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Dave Doyle 
 * 
 */

$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['0']="Marker zusammenfassen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['1']="Nah zusammenliegende Marker zusammenfassen (Clustering)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid']['0']="Cluster-Layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid']['1']="Wählen Sie das gewünschte Clusterlayout.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['0']="Clustergröße";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['1']="Je größer das Raster in dem Marker zusammengefasst werden.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['0']="Größte Zoomstufe";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['1']="Zoomstufe, bis zu der Marker zusammengefasst werden.";

$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['0']="Angabe des Namens erforderlich";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['1']="Sofern ein Formular zur E-Mail-Benachrichtigung des Mitglieds angezeigt wird, legen Sie hier fest, ob neben der E-Mail-Adresse auch der Name im Formular angegeben werden muss.";

$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_required']['0']="Bestätigung Datenschutz Geocoding";
$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_required']['1']="Bestätigung, dass die angegebene Adresse für das Geocoding an Google gesendet wird.";
$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_label']['0']="Label für die Checkbox";
$GLOBALS['TL_LANG']['tl_module']['cm_gc_acceptance_label']['1']="Label für die Checkbox";

$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_legend'] = "Datenschutz";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_required']['0']="Datenschutz-Bestätigung";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_required']['1']="Verhindert die Datenübertragung zu Google bis zur Bestätigung";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_text']['0']="Datenschutzhinweis";
$GLOBALS['TL_LANG']['tl_module']['cm_gm_acceptance_text']['1']="Datenschutzhinweis - Insert-Tags werden unterstützt.";

