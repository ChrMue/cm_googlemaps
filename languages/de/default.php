<?php 
/**
 * TL_ROOT/system/modules/cm_membergooglemaps/languages/de/default.php 
 * 
 * Contao extension: cm_membergooglemaps
 * 
 * Copyright : &copy; 2013 Christian Münster 
 * License   : GNU Lesser Public License (LGPL) 
 * Author    : Christian Münster (ChrMue) 
 * Translator: Christian Münster (ChrMue) 
 * 
 */
 
$GLOBALS['TL_LANG']['MSC']['cm_map_error'] = ">>> Eine Kartendarstellung ist leider derzeit nicht möglich. <<<";
$GLOBALS['TL_LANG']['CTE']['cm_mapPlaceholder'][0] = "Karten-Platzhalter";
$GLOBALS['TL_LANG']['CTE']['cm_mapPlaceholder'][1] = "zeigt die Karte als Contentelement eines Mitgliedes/einer Adresse und nicht direkt in den Detailinformationen an.";

//$GLOBALS['TL_LANG']['MSC']['list_perPage'] = "Ergebnisse pro Seite";
//$GLOBALS['TL_LANG']['MSC']['cm_map_getroute'] = "Route berechnen";
//$GLOBALS['TL_LANG']['MSC']['cm_map_getroutetable'] = "Route";
//$GLOBALS['TL_LANG']['MSC']['cm_map_getroutelist'] = "Route berechnen";
//$GLOBALS['TL_LANG']['MSC']['cm_map_topos'] = "Hierher";       
//$GLOBALS['TL_LANG']['MSC']['cm_map_frompos'] = "Von hier";
//$GLOBALS['TL_LANG']['MSC']['cm_map_fromaddr'] = "Startadresse";
//$GLOBALS['TL_LANG']['MSC']['cm_map_toaddr'] = "Zieladresse";
//$GLOBALS['TL_LANG']['MSC']['cm_radius_search'] = "Umkreissuche";
//$GLOBALS['TL_LANG']['MSC']['cm_lbl_location'] = "Adresse:";
//$GLOBALS['TL_LANG']['MSC']['cm_lbl_country'] = "Land:";
//$GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist'] = "Entfernung:";
//$GLOBALS['TL_LANG']['MSC']['cm_lbl_max_dist_drdn'] = "Umkreis auswählen:";
//$GLOBALS['TL_LANG']['MSC']['cm_distitem'] = "km";
//$GLOBALS['TL_LANG']['MSC']['cm_distsearch_label'] = "Suchen";
//$GLOBALS['TL_LANG']['MSC']['cm_distance'] = "km";
//$GLOBALS['TL_LANG']['MSC']['cm_email'] = "E-Mail:";
//$GLOBALS['TL_LANG']['MSC']['cm_captcha'] = "Spam-Schutz:";
//$GLOBALS['TL_LANG']['MSC']['cm_website'] = "Webseite:";
//$GLOBALS['TL_LANG']['MSC']['plzarea'] = "PLZ-Bereich:";
//$GLOBALS['TL_LANG']['MSC']['cm_plz_search'] = "PLZ-Suche";
//$GLOBALS['TL_LANG']['MSC']['coordspicker'] ="Position wählen";
//$GLOBALS['TL_LANG']['MSC']['cm_mapLng'] = "Längengrad";
//$GLOBALS['TL_LANG']['MSC']['cm_mapLat'] = "Breitengrad";
//$GLOBALS['TL_LANG']['MSC']['cm_map_visitorlocation'] = "Standort";
//$GLOBALS['TL_LANG']['MSC']['cm_name']="Ihr Name";
$GLOBALS['TL_LANG']['MSC']['cm_gc_privacyerror']="Sie müssen die Datenschutzbestimmungen akzepieren, um eine Umkreissuche durchzuführen.";
