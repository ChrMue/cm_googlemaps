<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package Cm_membergooglemaps
 * @link    http://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'ChrMue',
));  


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Models
	'ChrMue\cm_GoogleMaps\GoogleMapsClusterLayoutModel' => 'system/modules/cm_googlemaps/models/GoogleMapsClusterLayoutModel.php',
	'ChrMue\cm_GoogleMaps\GoogleMapsClusterStyleModel'  => 'system/modules/cm_googlemaps/models/GoogleMapsClusterStyleModel.php',
	'ChrMue\cm_GoogleMaps\GoogleMapsMapLayoutModel'  => 'system/modules/cm_googlemaps/models/GoogleMapsMapLayoutModel.php',
		// Modules
	// Classes
  	'ChrMue\cm_GoogleMaps\cm_GoogleMap_lib'    => 'system/modules/cm_googlemaps/classes/cm_GoogleMap_lib.php',
  	'ChrMue\cm_GoogleMaps\HelperGM'            => 'system/modules/cm_googlemaps/classes/HelperGM.php',
	'ChrMue\cm_GoogleMaps\ClusterLayout'       => 'system/modules/cm_googlemaps/classes/ClusterLayout.php',
	'ChrMue\cm_GoogleMaps\GoogleMap'           => 'system/modules/cm_googlemaps/classes/GoogleMap.php',
	'ChrMue\cm_GoogleMaps\MapLayout'           => 'system/modules/cm_googlemaps/classes/MapLayout.php',
	'ChrMue\cm_GoogleMaps\cm_googlemaps_lib'   => 'system/modules/cm_googlemaps/classes/cm_googlemaps_lib.php',

  	'ChrMue\cm_GoogleMaps\cm_mgm_fSockOpenConnect' => 'system/modules/cm_googlemaps/classes/base.php',
  	'ChrMue\cm_GoogleMaps\cm_mgm_cURLConnect' => 'system/modules/cm_googlemaps/classes/base.php',
  	'ChrMue\cm_GoogleMaps\GoogleMapsXMLData' => 'system/modules/cm_googlemaps/classes/base.php',

	'ChrMue\cm_GoogleMaps\ContentMapPlaceholder'  => 'system/modules/cm_googlemaps/elements/ContentMapPlaceholder.php',
  	'ChrMue\cm_GoogleMaps\cm_LatLngAjax'	=> 'system/modules/cm_googlemaps/classes/cm_LatLngAjax.php',
	// Widgets
	'ChrMue\cm_GoogleMaps\cm_MapPanel'		=> 'system/modules/cm_googlemaps/widgets/cm_MapPanel.php',
	'ChrMue\cm_GoogleMaps\cm_CoordPicker'	=> 'system/modules/cm_googlemaps/widgets/cm_CoordPicker.php',
	'ChrMue\cm_GoogleMaps\cm_LatLng'		=> 'system/modules/cm_googlemaps/widgets/cm_LatLng.php',

));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
    'cm_googlemap_js_main'          	=> 'system/modules/cm_googlemaps/templates',
    'mod_cm_googlemaps_map'  			=> 'system/modules/cm_googlemaps/templates',
	'be_mapwidget'                 		=> 'system/modules/cm_googlemaps/templates',
));
