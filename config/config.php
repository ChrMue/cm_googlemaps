<?php

/**
 * TYPOlight webCMS
 * Copyright (C) 2005 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at http://www.gnu.org/licenses/.
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009 
 * @author     Christian Muenster 
 * @package    CM_MemberGoogleMaps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
 */



array_insert($GLOBALS['BE_MOD']['design'], 2, array
(
	'cm_mapstyles' => array
	(
  		'tables'    => array('tl_cm_gmaplayout','tl_cm_gmaptypestyle'),
		'icon'      => 'system/modules/cm_googlemaps/assets/mapstyleicon.gif'
	),
	'cm_mapclusterstyles' => array
	(
  		'tables'    => array('tl_cm_gmapclusterlayout','tl_cm_gmapclusterstyle'),
		'icon'      => 'system/modules/cm_googlemaps/assets/clusterstyleicon.gif'
	)
));
if (TL_MODE == 'FE')
{
    $GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_googlemaps/assets/markerclusterer.js';
    //$GLOBALS['TL_JAVASCRIPT'][] = 'https://rawgit.com/googlemaps/js-marker-clusterer/gh-pages/src/markerclusterer.js';
} 
if (TL_MODE == 'BE')
{
	$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_googlemaps/assets/coordPicker.js'; 
	$GLOBALS['TL_CSS'][] = 'system/modules/cm_googlemaps/assets/cm_googlemapsBE.css'; 
}

$GLOBALS['TL_MODELS']['tl_cm_gmapclusterstyle']  = 'ChrMue\cm_GoogleMaps\GoogleMapsClusterStyleModel';
$GLOBALS['TL_MODELS']['tl_cm_gmapclusterlayout'] = 'ChrMue\cm_GoogleMaps\GoogleMapsClusterLayoutModel';

$GLOBALS['BE_FFL']['cm_LatLng'] 	 = 'ChrMue\cm_GoogleMaps\cm_LatLng';
$GLOBALS['BE_FFL']['cm_CoordPicker'] = 'ChrMue\cm_GoogleMaps\cm_CoordPicker';

$GLOBALS['TL_HOOKS']['executePostActions'][] = array('cm_GoogleMaps\cm_LatLngAjax', 'storePickedCoords');

$GLOBALS['TL_HOOKS']['addCustomRegexp'][] = array('cm_GoogleMaps\cm_GoogleMap_lib', 'addGeoCoordsRegexp');
$GLOBALS['TL_HOOKS']['addCustomRegexp'][] = array('cm_GoogleMaps\cm_GoogleMap_lib', 'addIntvalsAndDefaultRegexp');

$GLOBALS['TL_CTE']['includes']['cm_mapPlaceholder']='ChrMue\cm_GoogleMaps\ContentMapPlaceholder';


?>